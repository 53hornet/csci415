/* ttt.h
 *
 * Adam Carpenter - 2017
 */

#ifndef TTT_H
#define TTT_H


// Constants.
#define MESSAGE_BUFF 64
#define MAX_HOSTNAME 255
#define MAX_CLIENTS 4
#define FALSE 0
#define TRUE 1
#define X_WIN 'X'
#define O_WIN 'O'
#define TIE_GAME '-'

// Messages that can be sent by the server and client.
#define HANDLE "h"
#define ABORT "a"
#define PLAYERID 'p'
#define BOARD 'b'
#define MOVE "m"
#define WAIT "w"
#define ENDGAME 'e'

#endif

// Function declarations.
int sendMessage(int connSock, char *message);
int getMessage(int connSock, char *message);
