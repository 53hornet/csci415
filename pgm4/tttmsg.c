/* tttmsg.c
 *
 * Adam Carpenter - 2017
 */


#include "ttt.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int sendMessage(int connSock, char *message) {
    int left, num, put;
    left = MESSAGE_BUFF;
    put = 0;

    while (left > 0) {

        if ((num = write(connSock, message + put, left)) < 0) {
            // fprintf(stderr, "ttt/TTT: couldn't send message!\n");
            perror("ttt/TTT: couldn't send message because ");
            exit(1);
        }
        else if (num == 0) {
            return -1;
        }
        else {
            left -= num;
        }

        put += num;
    }

    return 0;
}

int getMessage(int connSock, char *message) {
    char ch;
    int i = 0;
    int num;

    for(i = 0; i < MESSAGE_BUFF; i++) {

        if ((num = read(connSock, &ch, 1)) < 0) {
            fprintf(stderr, "ttt/TTT: couldn't receive message!\n");
        }
        else if (num == 0) {
            return -1;
        }

        message[i] = ch;
    }

    return 0;
}
