#!/bin/sh
#
# boiler.sh
# Adam Carpenter - 2017 - acarpent - acarpenter@email.wm.edu
# Utilizes code from P. Kearns - 2004
#


# **** Initializations ****
tempFile="./beetle.temp"
dataFile="./beetle.data"
plotfile=""
lifetimeAwk='BEGIN { FS = " "; } NF == 11 {print $1 " " $11}'
usage="boiler: usage: ./boiler -n num -i min max step {-p plotfile.png} | -r statefile"
trap 'cleanExit' INT
trap 'giveCurrent' USR1
trap 'dumpState' USR2


# **** Function declarations ****
cleanExit() {
    kill -9 "$pid" 2>/dev/null
    rm -f $tempFile
    echo
    exit 0
}

giveCurrent() {
    echo $min
}

dumpState() {
    kill -9 "$pid" 2>/dev/null
    stateFile="./beetle.state"
    echo $num > $stateFile
    echo $min >> $stateFile
    echo $max >> $stateFile
    echo $step >> $stateFile
    echo $plotFile >> $stateFile
    exec 3<$tempFile

    while read -u 3 line; do
        echo "$line" >> $stateFile
    done

    exec 3<&-
    rm -f $tempFile
    echo
    exit 0
}

beetlejuice() {
    # beetlejuice
    # beetlejuice
    if [ -f ./beetle ]; then
        ./beetle $min $num >> $tempFile &
    else
        /home/f85/kearns/public/415/p6/beetle $min $num >> $tempFile &
    fi

    pid=$!

    while ps -p $pid --format pid= > /dev/null; do
        : # Just waiting for traps.
    done

    min=`expr $min + $step`
}

# **** Main script ****
# Remove any residual temp file (should be a nonissue).
rm -f $tempFile
rm -f $dataFile

# Make sure there are arguments.
if [ -z $1 ]; then
    echo $usage
    exit 1
fi

if [ $1 = "-r" ]; then # Operating in restore mode.
    shift

    if [ ! -e $1 ]; then
        echo boiler: state file not found
        exit 1
    fi

    # Get state file and read in beetle arguments.
    if [ ! -z $1 ]; then
        stateFile=$1
        exec 3<$stateFile
        read -u 3 line
        num=$line
        read -u 3 line
        min=$line
        read -u 3 line
        max=$line
        read -u 3 line
        step=$line
        read -u 3 line
        plotFile=$line

        while read -u 3 line; do
            echo "$line" >> $tempFile
        done

        exec 3<&-
    else
        echo $usage
        exit 1
    fi

else # Operating in normal mode.
    while [ "$1" != "" ]; do

        case $1 in

        # Get num.
        "-n")
        shift

        if [ ! -z $1 ]; then
            num=$1
        else
            echo $usage
            exit 1
        fi
        ;;

        # Get min, max, and step.
        "-i")
        shift

        if [ ! -z $1 ]; then
            min=$1
        else
            echo $usage
            exit 1
        fi

        shift

        if [ ! -z $1 ]; then
            max=$1
        else
            echo $usage
            exit 1
        fi

        shift

        if [ ! -z $1 ]; then
            step=$1
        else
            echo $usage
            exit 1
        fi
        ;;

        # Get plotfile if requested.
        "-p")
        shift

        if [ ! -z $1 ]; then
            plotFile=$1
        else
            echo $usage
            exit 1
        fi
        ;;

        # Catch extraneous args.
        *)
        echo $usage
        exit 1
        ;;

        esac

        shift
    done

fi

# Make sure required vars were set correctly.
if [ -z $num ] || [ -z $min ] || [ -z $max ] || [ -z $step ]; then
    echo $usage
    exit 1
fi

# I researched methods of checking whether a variable is an integer online
# for shell scripts (where variables aren't declared or strongly typed) and I
# found a method of comparing strings to regular expressions, where any
# character that isn't a numeric is incorrect.
case $num in
    ''|*[!0-9]*) echo $usage; exit 1;;
    *);;
esac

case $min in
    ''|*[!0-9]*) echo $usage; exit 1;;
    *);;
esac

case $max in
    ''|*[!0-9]*) echo $usage; exit 1;;
    *);;
esac

case $step in
    ''|*[!0-9]*) echo $usage; exit 1;;
    *);;
esac

# Run the first simulaton to ensure it gets done no matter the size of max.
beetlejuice

# Run the remaining beetle simulations.
while [ $min -lt $max ] || [ $min -eq $max ]; do
    beetlejuice
done

# Use awk to pull data from tempFile and print it out.
awk "$lifetimeAwk" "$tempFile"

# If a plot was requested, plot the data into the given filename.
if [ ! -z $plotFile ]; then
    awk "$lifetimeAwk" "$tempFile" >> $dataFile

    gnuplot <<- EOF
    set xlabel "Square Side (inches)"
    set ylabel "Mean Beetle Lifetime (seconds)"
    set term png
    set output "$plotFile"
    plot "$dataFile" using 1:2 title ""
EOF

    rm -f $dataFile
fi

rm -f $tempFile
