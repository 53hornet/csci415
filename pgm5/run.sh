#!/bin/bash

make
cp -f counter p5test/counter
cd p5test

./counter -b 4 -t 1 -d 0 -D 0 file0
./counter -b 2 -t 2 -d 1000 -D 1000 file0
./counter -b 10 -t 1 -d 0 -D 0 /usr/share/fortune/linuxcookie
./counter -b 2 -t 26 -d 0 -D 0 /usr/share/fortune/linuxcookie
./counter -b 2 -t 10 -d 0 -D 0 /usr/share/fortune/linuxcookie
./counter -b 2 -t 10 -d 0 -D 0 file{1,2,3,4,5,6}
./counter -b 1 -t 1 -d 0 -D 0 /usr/share/fortune/linuxcookie
