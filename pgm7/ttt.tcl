#!/usr/bin/tclsh
#
# ttt.tcl
#
# Adam Carpenter - 2017 - acarpent - acarpenter@email.wm.edu
# Utilizes code from P. Kearns

# **** Initialize Variables ****
global soundFlag; set soundFlag 1
global playerSide; set playerSide -
global playerHandle; set playerHandle (Player1)
global opponentHandle; set opponentHandle (Player2)
global myTurn; set myTurn 0


# **** Initialize Procedures****
proc resignMatch {} {
    global myTurn
    global playerSide

    if {$myTurn == 1} {
        set myTurn 0
        puts stdout R
        flush stdout
    }
}

proc soundToggle {} {
    global soundFlag
    set soundFlag [expr $soundFlag * -1]

    if {$soundFlag > 0} {
        .buttonFrame.soundButton configure -text "Silent"
    } else {
        .buttonFrame.soundButton configure -text "Sound"
    }

}

proc exitMatch {} {
    exit 0
}

proc playerMove {tag} {
    global myTurn
    global playerSide
    global array taken

    if {$myTurn == 1} {
        set myTurn 0
        puts stdout $tag
        flush stdout
    }

}

proc beep {} {
    global soundFlag

    if {$soundFlag > 0} {
        bell
        bell -nice
    }

}

# **** Main ****

# Create canvas.
canvas .gameCanvas -width 300 -height 300 -bg white

# Create board spaces.
.gameCanvas create rectangle 0    0    100  100 -fill white -outline white -tag 1
.gameCanvas create rectangle 100  0    200  100 -fill white -outline white -tag 2
.gameCanvas create rectangle 200  0    300  100 -fill white -outline white -tag 3
.gameCanvas create rectangle 0    100  100  200 -fill white -outline white -tag 4
.gameCanvas create rectangle 100  100  200  200 -fill white -outline white -tag 5
.gameCanvas create rectangle 200  100  300  200 -fill white -outline white -tag 6
.gameCanvas create rectangle 0    200  100  300 -fill white -outline white -tag 7
.gameCanvas create rectangle 100  200  200  300 -fill white -outline white -tag 8
.gameCanvas create rectangle 200  200  300  300 -fill white -outline white -tag 9

# Bind board spaces with rectangles.
.gameCanvas bind 1 <Button-1> {playerMove 1}
.gameCanvas bind 2 <Button-1> {playerMove 2}
.gameCanvas bind 3 <Button-1> {playerMove 3}
.gameCanvas bind 4 <Button-1> {playerMove 4}
.gameCanvas bind 5 <Button-1> {playerMove 5}
.gameCanvas bind 6 <Button-1> {playerMove 6}
.gameCanvas bind 7 <Button-1> {playerMove 7}
.gameCanvas bind 8 <Button-1> {playerMove 8}
.gameCanvas bind 9 <Button-1> {playerMove 9}

# Draw board grid.
.gameCanvas create line 100 0 100 300 -width 4
.gameCanvas create line 200 0 200 300 -width 4
.gameCanvas create line 0 100 300 100 -width 4
.gameCanvas create line 0 200 300 200 -width 4

pack .gameCanvas

# Set up game status frame and labels.
frame .statusFrame -width 300 -height 100 -bg grey
pack .statusFrame -expand 1 -fill x
label .statusFrame.playerStatus  -text "?" -bg grey -fg white
label .statusFrame.vs -text "VS" -bg grey -fg white
label .statusFrame.opponentStatus  -text "?" -bg grey -fg white
label .statusFrame.gameStatus  -text "\nAwaiting match..." -bg grey -fg white
pack .statusFrame.gameStatus -expand 1 -fill x -side bottom
pack .statusFrame.playerStatus -expand 1 -fill x -side left
pack .statusFrame.vs -expand 1 -fill x -side left
pack .statusFrame.opponentStatus -expand 1 -fill x -side left


# Set up button frame and buttons.
frame .buttonFrame
pack .buttonFrame -expand 1 -fill x
button .buttonFrame.soundButton -text "Silent" -command soundToggle
button .buttonFrame.resignButton -text "Resign" -command resignMatch -state disabled
button .buttonFrame.exitButton -text "Exit" -command exitMatch -state disabled
pack .buttonFrame.soundButton .buttonFrame.resignButton .buttonFrame.exitButton -expand 1 -fill x -side left

bell
