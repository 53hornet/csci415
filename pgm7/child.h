#ifndef _mdw_CHILD_H
#define _mdw_CHILD_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>

int start_child(char *cmd, FILE **readpipe, FILE **writepipe); 
void i_am_the_master_commander(char *handle, char *opphandle, FILE **writeTo);

#endif
