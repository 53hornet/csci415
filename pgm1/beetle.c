#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<errno.h>

/* 
 * The purpose of this program is to simulate and record the average lifetimes
 * of beetles on cardboard squares, suspended over vats of boiling oil. It takes
 * in two parameters, both positive, long integers. The first integer refers
 * to the length of one side of the cardboard square, in inches. The second
 * refers to the number of beetles (number of times) with which to run this 
 * simulation. 
 *
 * usage: ./beetle int int (./beetle square_size number_of_beetles)
 */

long double simulateLifetime(long int squareSize, long int beetleCount) {
	/* 
	 * simulateLifetime returns the collective lifetime of the beetles
	 * suspended over the vat of oil. If this function fails to simulate any
	 * beetles, it will return -1. 
	 *
	 * This simulation places a beetle at the center of a cardboard square. 
	 * The cardboard square is represented by a coordinate grid, with the origin
	 * being at the top-left corner, or the "home position". The 
	 * beetle picks a random angle to move in, represented by random(),and moves 
	 * forward 1 inch. This movement takes 1 second. The beetle waits 1 more
	 * second before starting the whole process over again. Once the beetle's
	 * coordinates move off the bounds of the coordinate grid, it "falls into
	 * the vat" and the simulation continues with the next beetle, decreasing 
	 * the count.
	 * 
	 * long int squareSize - represents the size of one side of the cardboard 
	 * square.
	 * 
	 * long int beetleCount - represents the number of beetles to simulate, or 
	 * rather, the number of times to run the simulation.
	 */
	
	long double beetleX;
	long double beetleY;
	long double lifetime;
	double angle;
	long int count;
	long int seconds;
	
	lifetime = -1;
	count = beetleCount;
	seconds = 0;
		
	while (count > 0) {
		beetleX = squareSize / 2;
		beetleY = squareSize / 2;
		seconds = 0;
		
		while (beetleX >= 0 && beetleY >= 0 && beetleX <= squareSize 
			&& beetleY <= squareSize) {
			angle = random();
			beetleX += cos(angle);
			beetleY += sin(angle);
			seconds++;
			
			if (beetleX >= 0 && beetleY >= 0 && beetleX <= squareSize 
				&& beetleY <= squareSize) {
				seconds++; // the beetle is resting but hasn't fallen off edge
			}
		}
		
		lifetime += seconds;		
		count--;
	}

	return lifetime;
}

int main(int argc, char *argv[]) {
	/* 
	 * main sets up the beetle program. It reads the user's input and checks it
	 * to make sure that it is valid. It converts the input to values that the
	 * simulation function can use and then calls simulateLifetime(). main then
	 * prints the output to the console.
	 */
	long double lifetime;
	long int squareSize;
	long int beetleCount;
	char *squareTest;
	char *countTest;
	
	// Check for improper number of arguments
	if (argc != 3) {
		fprintf(stderr, "beetle: usage: ./beetle int int\n");
		exit(1);
	} 
	
	errno = 0; // Reset error status
	squareSize = strtol(argv[1], &squareTest, 10);
	beetleCount = strtol(argv[2], &countTest, 10);
	
	// Check for invalid input 
	if (squareTest == argv[1] || countTest == argv[2]) { 
		fprintf(stderr, "beetle: usage: ./beetle int int\n");
		exit(1);
	}
	
	// Check for negative input
	if (squareSize < 0 || beetleCount < 0) { 
		fprintf(stderr, "beetle: error: input must be positive integers\n");
		exit(1);
	}
	
	// Check for overflow/underflow
	if (errno == ERANGE) { 
		fprintf(stderr, "beetle: error: int too large\n");
        exit(1);
	}
	
	// If no square to use for simulation then finish
	if (squareSize == 0) {
		fprintf(stderr, "No square given so lifetime is 0\n");
		exit(0);
	}
	
	// If no beetles given then finish
	if (beetleCount == 0) {
		fprintf(stderr, "No beetles given, so lifetime is 0\n");
		exit(0);
	}
	
	// Run simulation and check return value
	if ((lifetime = simulateLifetime(squareSize, beetleCount)) == -1) { 
		fprintf(stderr, "beetle: error: beetles have achieved immortality!\n");
		exit(1);
	}
	
	// Take the average of the beetles' lifetime and print the output. 
	lifetime = lifetime / beetleCount; 
	printf("%li by %li square, %li beetles, mean beetle lifetime is %.1Lf\n", 
		squareSize, squareSize, beetleCount, lifetime);
	exit(0);
}
